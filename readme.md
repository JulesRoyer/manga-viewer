# Site de visionnage de Mangas (entre autres) [En cours]
## But
Ce site a pour but plusieurs choses :
- Nous permettre de visionner des Mangas (en lien avec [down-mangas](https://gitlab.com/JulesRoyer/down-mangas))
- S'entraîner en CSS, Javascript, PHP
- S'entraîner sur les aspects serveur

## TODO
- Un CSS plus joli à notre image
- Comptes utilisateurs (si possible connexion sécurisée)
- Chat/Forum en markdown en utilisant [parsedown](https://github.com/erusev/parsedown)
- [Sur le serveur] Essayer de passer en httpS
- Peut-être essayer le Bootstrap
- À suivre, le site est là pour ça