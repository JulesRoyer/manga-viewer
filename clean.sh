#! /bin/bash

rm -rf .DS_Store ._*

cd Mangas

racine=`pwd`

for D in $(find . -type d | tr ' ' '§')
do
    cd "$(echo "$D" | tr '§' ' ')"
    rm -rf .DS_Store ._*
    cd "$racine"
done