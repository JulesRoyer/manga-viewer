<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/beau.css">
		<title>Lecteur mangas</title>
	</head>

	<body>
		<?php
			$manga = $_GET['m']; 
			$chapitre = $_GET['c'];
			$dir = './Mangas/'.$manga.'/'.$chapitre.'';
			// Ajouter condition d'aleatoire ou de reprise depuis un cookie
			$contenu = array_diff(scandir($dir), array('..', '.'));
			$contenu["0"] = $manga;
			$contenu["1"] = $chapitre;
			echo '<p id="pages" style="display:none">'.json_encode($contenu).'</p>';
		?>
		<nav id="navbar">
			<ul>
				<li><a href="index.html">Accueil</a></li>
				<li><a href="#" class="active">Lecteur</a></li>
				<li><a href="compte.html">Mon compte</a></li>
				<li><a href="forum.html">Forum / Tchat</a></li>
			</ul>
		</nav>

		<div class="coucou">
			<img src="img/easteregg.png" alt="Luffy" />
		</div>

		<div id="Contenu" ng-app="monApp" ng-controller="monControlleur">
			<div id="Corps">
				<div class="barre">Lecteur</div>
				<div id="Image" image-man></div>
			</div>
		</div>

		<footer></footer>
		<script type="text/javascript" src="js/angular.js"></script>
		<script type="text/javascript" src="js/lecteur.js"></script>
	</body>
</html>